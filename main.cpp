using namespace std;
#include <iostream>
#include <ctime>
#include <cstdlib>

int G = 9;

const int n = 28887;

// 1: 124500000
// 2: 28887
// 3: 2147483646

struct TabInterface {
    char litera;
    int liczba;
};

TabInterface A[n];
TabInterface B[n];

char getRandomChar() {
    return 'A' + rand() % 26;
}

int getRandomInt(int limit) {
    return rand() % (limit + 1);
}

void printLastTwenty(TabInterface printMe[], int size) {
    int from = 0;
    int to = size;

    if (size > 20) {
        from = size - 21;
    }

    for (int i = from; i < to; ++i) {
        cout << i << " "
             << printMe[i].litera
             << printMe[i].liczba << " "
             << endl;
    }
}

void printStats(TabInterface printMe[], int size) {
    cout << "rozmiar tablicy: " << size << endl
         << "zajetosc pamieci: " << sizeof(printMe) * size << endl;
}

void fillArrayIterate(TabInterface tab[], int size) {
    for (int i = 0; i < size; ++i) {
        tab[i].liczba = getRandomInt(G);
        tab[i].litera = getRandomChar();
    }
}

void fillByRecurence(TabInterface B[], int index) {
    if (index < n) {
        B[index].liczba = getRandomInt(G);
        B[index].litera = getRandomChar();
        fillByRecurence(B, index + 1);
    }
}

void staticIterateArray(TabInterface A[]) {
    fillArrayIterate(A, n);
    printLastTwenty(A, n);
    printStats(A, n);
}

void staticRecurenceArray(TabInterface B[]) {
    fillByRecurence(B, 0);
    printLastTwenty(B, n);
    printStats(B, n);
}

void dynamicArray() {
    static int k = 0;

    TabInterface *D;

    cout << "podaj rozmiar tablicy" << endl;
    cin >> k;

    D = new TabInterface[k];

    fillArrayIterate(D, k);
    printLastTwenty(D, k);
    printStats(D, k);

    delete D;
}

int main() {
    int type = 0;

    srand(time(NULL));
    cout << "wybierz co chcesz zrobic:"
         << endl
         << "1: zwykla tablica statyczna A[n] interacjna" << endl
         << "2: zwykla tablica statyczna B[n] rekurencyjna" << endl
         << "3: tablica dynamiczna na podstawie podanej zmiennej" << endl;

    cin >> type;

    switch (type) {
        case 1:
            staticIterateArray(A);
            break;
        case 2:
            staticRecurenceArray(B);
            break;
        case 3:
            dynamicArray();
            break;
        default:
            cout << "game over, try again" << endl;
            main();
    }

    return 0;
}
