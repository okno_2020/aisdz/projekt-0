# Temat p0 – za 1 pkt

Napisać program, który w zależności od wyboru użytkownika realizuje **jedną z trzech operacji**
służących do wylosowania w trzech tablicach **rekordów** o polach typu **(char, int)** i wartościach z
zakresu (duża litera, liczba całkowita od 0 do stałej G) :

1. Zwykłą **tablicę statyczną A[n]** (n – stała) wypełnia wylosowanymi rekordami przy użyciu
   __zwykłej funkcji iteracyjnej_.
2. Zwykłą tablicę statyczną B[n] (n - stała) wypełnia wylosowanymi rekordami przy użyciu
   **funkcji rekurencyjnej**.
3. Wczytuje zmienną całkowitą k i przydziela pamięć na **tablicę dynamiczną D o rozmiarze k**,
   po czym wypełnia ją wylosowanymi rekordami przy użyciu **zwykłej funkcji iteracyjnej**.
   
Po zakończeniu wypełniania dowolnym z tych sposobów program drukuje:

* w kolejnych wierszach, wraz z indeksami, 20 ostatnich rekordów tak wylosowanej tablicy
   (przy użyciu zwykłej funkcji iteracyjnej drukującej tablicę)
* rozmiar tablicy i wyznaczony na tej postawie rozmiar pamięci zajętej przez tablicę.
   
Należy **uzyskać maksymalny rozmiar tablicy** dla każdej wersji działania programu. W tym celu
   należy zwiększać ręcznie w kodzie programu stałą n (w wersji 1 i 2) lub wprowadzaną zmienną k
   (w wersji 3) i uruchamiać program aż do momentu, gdy okaże się, że wyniki się nie drukują, bo
   program się zawiesza (tablica zajmie zbyt dużo pamięci statycznej lub dynamicznej albo też
   nastąpi przepełnienie stosu w przypadku losowania rekurencyjnego).

Uwagi:

1. Uzyskane wartości maksymalne dla każdej z wersji należy wysłać do prowadzącego w mailu
   wraz z kodem programu.
2. **Wersja 2 nie jest obowiązkowa**, nie wpływa na ocenę programu, ale warto ją napisać dla
   własnej satysfakcji.
   
Wskazówki:
   
1. Wypełnienie tablicy z wykorzystaniem funkcji rekurencyjnej należy wykonać na podstawie
   lekcji 1 i zadań końcowych do niej.
2. Należy pamiętać o zwolnieniu przydzielonej pamięci dynamicznej.
3. Program należy napisać w klasycznym standardzie strukturalnego języka C/C++, używanym
   na Programowaniu, bez korzystania z możliwości C++11 czy biblioteki STL. 
